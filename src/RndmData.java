import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RndmData {
    PrintWriter save;
    Random los = new Random(); 
    final int ilosc_rekordow = 10000;
    final int ilosc_ocen = 10;
    
    RndmData(){
        save();
    }
    
    void save(){
        try {
            save = new PrintWriter("dane.txt");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RndmData.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        save.println(ilosc_rekordow + " " + ilosc_ocen);
        for (int j = 0; j < (ilosc_rekordow*ilosc_ocen); j++) {           
            save.println( (double)(los.nextInt(11)+2) / 2 );
        }
        save.close();
    }
    
    public static void main(String[] args) {
        //int s=ilosc_rekordow*ilosc_ocen;
        new RndmData();
    }
}
